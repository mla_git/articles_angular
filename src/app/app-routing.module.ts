import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { ArticleFullViewComponent } from './article-full-view/article-full-view.component';
import { ArticleFormComponent } from './article-form/article-form.component';

const routes: Routes = [
{ path: 'articles', component: ArticlesListComponent },
{ path: 'add', component: ArticleFormComponent },
{ path: 'fullView/:id', component: ArticleFullViewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
