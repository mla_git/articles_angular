import { Injectable } from '@angular/core';
import { Article } from '../model/article';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

	private url: string;

  constructor(private http: HttpClient) {
    this.url = 'http://localhost:8080/';
  }
  
  public findAll(): Observable<Article[]> {
    return this.http.get<Article[]>(this.url + 'articles');
  }
  public findById(id: number): Observable<Article> {
    return this.http.get<Article>(this.url + 'articleById/' + id);
  }
   public findSources(): Observable<string[]> {
    return this.http.get<string[]>(this.url + 'sources');
  }
   public findEditions(): Observable<string[]> {
    return this.http.get<string[]>(this.url + 'editions');
  }
   public findRegions(): Observable<string[]> {
    return this.http.get<string[]>(this.url + 'regions');
  }
   public findDepartements(): Observable<string[]> {
    return this.http.get<string[]>(this.url + 'departements');
  }
   public findSecteurs(): Observable<string[]> {
    return this.http.get<string[]>(this.url + 'secteurs');
  }
   public findThemes(): Observable<string[]> {
    return this.http.get<string[]>(this.url + 'themes');
  }
    public save(article: Article) {
    return this.http.post<Article>(this.url + 'addArticle', article);
  }
    
}
