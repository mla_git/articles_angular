import { Component, OnInit } from '@angular/core';
import { Article } from '../model/article';
import { ArticlesService } from '../service/articles.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css']
})
export class ArticleFormComponent implements OnInit {

	article: Article;
	sources: string[];
	editions: string[];
	departements: string[];
	regions: string[];
	secteurs: string[];
	themes: string[];

  constructor(private route: ActivatedRoute, private router: Router,private articlesService: ArticlesService) { 
	this.article = new Article();
	
  }
  onSubmit() {
    this.articlesService.save(this.article).subscribe(result => this.gotoArticleList());
  }

    gotoArticleList() {
    this.router.navigate(['/articles']);
  }
  
  
  ngOnInit() {
	  this.articlesService.findSources().subscribe(data => {
      this.sources = data;
    });
	  this.articlesService.findEditions().subscribe(data => {
      this.editions = data;
    });
	  this.articlesService.findDepartements().subscribe(data => {
      this.departements = data;
    });
	  this.articlesService.findRegions().subscribe(data => {
      this.regions = data;
    });
	  this.articlesService.findSecteurs().subscribe(data => {
      this.secteurs = data;
    });
	  this.articlesService.findThemes().subscribe(data => {
      this.themes = data;
    });	
  }

}
