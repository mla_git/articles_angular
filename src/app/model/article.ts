export class Article {
	id: string;
    titre: string;
    corps: string;
	publicationDate: string;
	source: string;
	edition: string;
	departements: string;
	regions: string;
	secteurs: string;
	themes: string;
}
