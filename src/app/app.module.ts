import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { ArticlesService } from './service/articles.service';
import { ArticleFullViewComponent } from './article-full-view/article-full-view.component';
import { ArticleFormComponent } from './article-form/article-form.component';
import { FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material' ;
import { MatPaginatorModule } from '@angular/material';
import { MatSortModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import {MatInputModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    ArticlesListComponent,
    ArticleFullViewComponent,
    ArticleFormComponent
  ],
  imports: [
    BrowserModule,
	FormsModule,
    AppRoutingModule,
	HttpClientModule,
	MatTableModule,
	MatPaginatorModule,
	MatSortModule,
	BrowserAnimationsModule,
	MatFormFieldModule,
	MatInputModule
  ],
  providers: [ArticlesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
