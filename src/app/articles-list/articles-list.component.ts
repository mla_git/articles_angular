import { Component, OnInit , ViewChild} from '@angular/core';
import { Article } from '../model/article';
import { ArticlesService } from '../service/articles.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.css']
})
export class ArticlesListComponent implements OnInit {
	
	displayedColumns = ['titre', 'publicationDate', 'source'];
	dataSource: MatTableDataSource<Article>;
	
	@ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: false}) sort: MatSort;
	
	articles: Article[];

  constructor(private articlesService: ArticlesService) { }
  
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  
  ngOnInit() {
	  this.articlesService.findAll().subscribe(data => {
      this.articles = data;
	  this.dataSource = new MatTableDataSource(this.articles);
	  this.dataSource.sort = this.sort;
	 this.dataSource.paginator = this.paginator;
    });
  }

}
