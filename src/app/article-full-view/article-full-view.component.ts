import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../model/article';
import { ArticlesService } from '../service/articles.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-article-full-view',
  templateUrl: './article-full-view.component.html',
  styleUrls: ['./article-full-view.component.css']
})
export class ArticleFullViewComponent implements OnInit {

	@Input()
    articleId: number;
	
	article: Article;

  constructor(private articlesService: ArticlesService, private route: ActivatedRoute) { }

  ngOnInit() {
	  this.route.params.subscribe(params => {
      this.articleId = +params['id'];
	  });
	  this.articlesService.findById(this.articleId).subscribe(data => {
      this.article = data;
    });
  }

}
